#!/usr/bin/env python3

import os
import sys
import argparse
import json
from datetime import datetime, date, timedelta
from minio import Minio
from minio.error import ResponseError

class Directory(object):    
    def __init__(self, name):
        self.name = name
        self.hasBox = False
        self.toDelete = False
        self.firstOfWeek = False
        
        # parse date
        self.date = datetime.strptime(name, '%Y-%m-%d-%H%M%S-%f/').date()
        self.yearCW = "%d.%d" % (self.date.isocalendar()[:2])
        self.yearMonth = self.date.strftime("%Y.%m")
    
    def __repr__(self):
        return "<Directory %s, Date: %s, yearCW: %s hasBox: %r, toDelete: %r>" % (
                self.name, self.date, self.yearCW, self.hasBox, self.toDelete)
        
    def __str__(self):
        return "Name: %s, Date: %s, yearCW: %s, hasBox: %r, toDelete: %r" % (
                self.name, self.date, self.yearCW, self.hasBox, self.toDelete)

def main(arguments):
    ## Parse arguments
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--host', help="Host", metavar=("<name>", "<accessKey>", "<secretKey>"), type=str, nargs=3)
    parser.add_argument('--nointeraction', help="Run without interaction", dest="interaction", action="store_false")
    parser.add_argument('bucket', help="Bucket name (when using mc config host name has to be equal to bucket name)", type=str)    

    args = parser.parse_args(arguments)
    
    # get host parameters
    if args.host is None:
        config = os.popen("mc config --json host list " + args.bucket).read()
        config = json.loads(config)
        host = config["URL"]
        host = host.replace("http://", "")
        host = host.replace("https://", "")
        accessKey = config["accessKey"]
        secretKey = config["secretKey"]
    else:
        host = args.host[0]
        accessKey = args.host[1]
        secretKey = args.host[2]

    # today
    today = date.today()

    ## Create minio client
    client = Minio(host, accessKey, secretKey, secure=False)
    
    # Test existance of bucket
    try:
        client.bucket_exists(args.bucket)
    except ResponseError as err:
        print(err)
        
    ## Get list of objects in bucket
    try:
        objects = client.list_objects(args.bucket, recursive=False)
    except ResponseError as err:
        print(err)
    
    ## Create list of directories
    directories = [] 
    for obj in objects:
        if obj.is_dir and not obj.object_name == 'snapshot/':
            d = Directory(obj.object_name)
            directories.append(d)        
    
    ## Check if directories have box backup
    for d in directories:
        try:
            objects = client.list_objects(args.bucket, prefix=d.name, 
                                          recursive=False)
        except ResponseError as err:
            print(err)
        for obj in objects:
            if 'box' in obj.object_name:
                d.hasBox = True
                
    ## Find first box backup of each (year, month, week)
    # Get a list of (years, months, weeks)
    years = []
    months = []
    weeks = []
    for d in directories:
        years.append(d.date.year)
        months.append(d.yearMonth)
        weeks.append(d.yearCW)
    years = set(years)
    months = set(months)
    weeks = set(weeks)
    
    # Aggregate backups to years and find backups to keep
    yearBkpsToKeep = []
    for year in years:
        yearDirs = []
        for d in directories:
            if d.hasBox and d.date.year == year:
                yearDirs.append(d)
        oldestDir = yearDirs[0]
        for d in yearDirs:
            if d.date < oldestDir.date:
                oldestDir = d
        yearBkpsToKeep.append(oldestDir.name)
 
    # Keep 2 monthly backups => 1 additional month (i.e. remove older months)
    if len(months) > 2:
        months = sorted(months, reverse=True)[:2]
        
    # Aggregate backups to months and find backups to keep
    monthBkpsToKeep = []
    for month in months:
        monthDirs = []
        for d in directories:
            if d.hasBox and d.yearMonth == month:
                monthDirs.append(d)
        oldestDir = monthDirs[0]
        for d in monthDirs:
            if d.date < oldestDir.date:
                oldestDir = d
        monthBkpsToKeep.append(oldestDir.name)
                
    # Keep 5 weekly backups => 4 additional weeks (i.e. remove older weeks)
    if len(weeks) > 5:
        weeks = sorted(weeks, reverse=True)[:5]
    
    # Aggregate backups to weeks and find backups to keep
    weekBkpsToKeep = []
    for week in weeks:
        weekDirs = []
        for d in directories:
            if d.hasBox and d.yearCW == week:
                weekDirs.append(d)
        oldestDir = weekDirs[0]
        for d in weekDirs:
            if d.date < oldestDir.date:
                oldestDir = d
        weekBkpsToKeep.append(oldestDir.name)
        
    ## Define directories to delete: 
    # a: only backups older than 6 days
    # b1: all not weekBkpsToKeep and not monthBkpsToKeep and not yearBkpsToKeep
    #    backups
    # b2: all not box backups
    for d in directories:
        delta = today - d.date
        # a:
        if delta.days > 6:
            # b1:
            if not d.hasBox:
                d.toDelete = True
            
            # b2:
            if (not d.name in weekBkpsToKeep \
                and not d.name in monthBkpsToKeep \
                and not d.name in yearBkpsToKeep):
                d.toDelete = True
        
    ## Show some statistics before deleting
    numWithBox = 0
    numBkpsToKeep = 0
    for d in directories:
        if d.hasBox:
            numWithBox = numWithBox + 1
        if not d.toDelete:
            numBkpsToKeep = numBkpsToKeep + 1
    print("Found:\n%d backups at all\n%d backups with box\n" % (len(directories), 
            numWithBox))
    print("%d years to keep: " % (len(years)))
    print(sorted(years))
    print("%d months to keep: " % (len(months)))
    print(sorted(months))
    print("%d weeks to keep: " % (len(weeks)))
    print(sorted(weeks))
    print("%d backups to keep" % (numBkpsToKeep))
    print("%d backups to delete\n" % (len(directories) - numBkpsToKeep))    
            
    ## Delete those directories
    if (len(directories) - numBkpsToKeep) > 0:
        print("Deleting: should following backups be removed?")
        for d in directories:
            if d.toDelete == True:
                print(" - %s %s" % (d.name, '(has box) ' if d.hasBox else ''))
                
        if args.interaction:
            shouldRemove = input("Remove [yes/no]? ")
        else:
            shouldRemove = "yes"
            
        if shouldRemove.lower() == "yes" or shouldRemove.lower() == "no":
            if shouldRemove.lower() == "yes":
                print("Deleted:")
                for d in directories:
                    if d.toDelete == True:
                        try:
                            objects = client.list_objects(args.bucket, 
                                                    prefix=d.name, recursive=True)
                            for obj in objects:
                                client.remove_object(args.bucket, obj.object_name)
                            print(" - %s %s" % (d.name, '(has box) ' if d.hasBox else ''))
                        except ResponseError as err:
                            print(err)
                print("[done]")
        else:
            raise Exception("Bad answer")
            
if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
