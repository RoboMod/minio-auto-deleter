# Minio Auto Deleter

Mirror of internal Minio auto deleter project. 

## Requirements

To use this script, you need a Python3 environment with the minio client python library.

## Usage

This projects aim is to automatically delete Cloudron backups in a remote Minio instance. The script connects to minio and filters the directories in the given bucket. All dirs with a "box" subdir are marked as full backups. Afterwards, the backups to keep are chosen: 1 yearly, 2 monthly, 5 weekly, 7 dayly backups and the non-full backups of the last week (identical ones are only keep once). All other backups are deleted from the remote minio.

The script can be used interactively or using a systemd service. The login credentials can either be set by the parameter "--host" or using the minio client configuraton. The minio client configuration can be defined using the ``mc`` command or directly in the config file. Keep in mind that the config file is stored in the home directory, so it depends on the user executing the script. 

To run the script as systemd service, you can use the provided timer running every 12 hours. The service (and the timer) gets the name used in the minio client configuration like ``auto-deleter@<mc-config-name>.service`` (see systemd usage of template units). 